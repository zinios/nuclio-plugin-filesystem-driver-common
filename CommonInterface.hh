<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/

namespace nuclio\plugin\fileSystem\driver\common
{
	use nuclio\core\plugin\Plugin;	
	
	interface CommonInterface
	{
		/**
		 * Check if the file exist or not.
		 * @param  string $path Path to the file.
		 * @return bool       	True/False for file existance.
		 */
		public function exists(string $path):bool;

		/**
		 * Check if the path given is a file.
		 * @param  string  $path Path to the file.
		 * @return boolean       True/False for file or not.
		 */
		public function isFile(string $path):bool;

		/**
		 * Check whether the param given is path or not.
		 * @param  string  $directory Path to a directory
		 * @return boolean            True/False for the path or not.
		 */
		public function isDirectory(string $directory):bool;

		/**
		 * Checks if the passed folder is empty
		 * @param  string  $directory path to folder to check
		 * @return boolean            True/False on empty or not
		 */
		
		public function isEmpty(string $directory):bool;
		/**
		 * Check if the given path is readable.
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isReadable(string $path):bool;

		/**
		 * Check if the given path is writeable.
		 * @param  string  $path Path of directory
		 * @return boolean       True/False for writable or not.
		 */
		public function isWritable(string $path):bool;

		/**
		 * Delete file.
		 * @param  string $path Path to the file to be deleted.
		 * @return mixed       	True if delete success, error message if failed.
		 */
		public function delete(string $path, int $rules):bool;

		/**
		 * Move file to another place.
		 * @param  string $path   	File to be moved.
		 * @param  string $target 	Where to move.
		 * @return bool         	True/False for the move process.
		 */
		public function move(string $path, string $target, int $rules):bool;

		/**
		 * Copy file to another place.
		 * @param  string $path   	File to be copied.
		 * @param  string $target 	Where to copied.
		 * @return bool         	True/False for the copied process.
		 */
		 
		public function copy(string $path, string $target):bool;

		/**
		 * Creates a new directory at the given path
		 * @param string $path 		Path to be created.
		 * @return  bool  			Treu/false on success/failure
		 */
		public function create(string $path):bool;

		/**
		 * Retruns the contents of the folder at the passed path
		 * @param  string $path 
		 * @return string       contents of the given path
		 */
		public function listAllFilesAndFolders(string $path):Vector<string>;

		/**
		 * Returns a list of all the files/folders matching the given regex string inside the given path.
		 * @param  string $path  Path to perform the search into.
		 * @param  string $regex regex expression to be matched
		 * @return string List of all the search results
		 */
		public function find(string $path, string $regex):Vector<string>;

		/**
		 * Get content of a file.
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */
		public function read(string $path):string;

		/**
		 * Write content to a file.
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */
		public function write(string $path, string $contents):bool;

		/**
		 * Appends content to a file.
		 * @param  string $path Path to the file.
		 * @return mixed       	Return file content if exist and error if file does not exist.
		 */
		public function append(string $path, string $contents):mixed;

		/**
		 * Creates an empty file at hte passed path
		 * @param  string $path the path to the file to be created
		 * @return bool       	true/false on success/failure
		 */
		public function touch(string $path):bool;

		/**
		 * Get the file name.
		 * @param  string $path Path to the file.
		 * @return string      	File name
		 */
		public function getName(string $path):?string;

		/**
		 * Get the file extension.
		 * @param  string $path Path to the file.
		 * @return string      	File extension
		 */
		public function getExt(string $path):?string;

		/**
		 * Get the file type.
		 * @param  string $path Path to the file.
		 * @return string      	File type
		 */
		public function getType(string $path):?string;

		/**
		 * Get the file size.
		 * @param  string $path Path to the file.
		 * @return int      	File size
		 */
		public function getSize(string $path):?int;
	}
}
